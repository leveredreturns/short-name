dist:
	@echo 'making dist'
	@rm -rf ./dist
	@node_modules/.bin/babel lib -d dist

lint:
	@eslint .

test:
	@./node_modules/.bin/mocha ./tests

.PHONY: dist lint test
