
0.1.0 / 2017-11-13
==================

  * upgrade packages; fix 'Bank of America'
  * Update README.md

0.0.10 / 2016-04-20
==================

  * update distribution bundle build
  * update dist

0.0.9 / 2016-04-04
==================

  * minor improvements

0.0.8 / 2016-04-02
==================

  * update dist

0.0.7 / 2016-04-02
==================

  * improve case/punctuation robustness, iterative replacement, more replacement rules

0.0.6 / 2016-01-05
==================

  * update dist

0.0.5 / 2016-01-05
==================

  * Partners L.P. replace

0.0.4 / 2015-12-06
==================

  * change api to return this
  * find has the option to overwrite this.data for better chaining

0.0.3 / 2015-12-06
==================

  * add reduce feature

0.0.2 / 2015-12-04
==================

 - add unique function
 - fix object reference; now create a new array in this.data

0.0.1 / 2015-12-02
==================

- functioning Series class
