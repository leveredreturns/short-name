/**
 * Export shorten
 */

module.exports = (name) => {
  var orig = name
  var short = shorten(name)
  while (short !== name) {
    name = short
    short = shorten(name)
  }
  return short || orig
}

/**
 * Shorten a company name
 */

function shorten (name) {
  return name.trim()
    .replace(/^The /, '')                                   // The
    .replace(/ [Cc]lass [A-Za-z]$/, '')                     // Class X
    .replace(/ [Cc]ommon [CS]$/, '')                        // Common S|C
    .replace(/ [Cc]ommon [Ss][t]?[o]?[c]?[k]?[s]?$/, '')    // Common Stock
    .replace(/[,]? [Ii]ncorporated$/, '')                   // Incorporated
    .replace(/[,]? [Ll]imited$/, '')                        // Limited
    .replace(/[,&]? [Cc]ompan(y|ies)$/, '')                 // Company
    .replace(/[,]? [Cc]orporation[s]?$/, '')                // Corporation
    .replace(/[,&]? [Cc]orp[s]?[.]?$/, '')                  // Corp
    .replace(/[,&]? [Cc]o[s]?[.]?$/, '')                    // Co
    .replace(/[,]? [Hh]olding[s]?$/, '')                    // Holding
    .replace(/[,]? [Gg]roup[s]?$/, '')                      // Group
    .replace(/[,]? [Ee]nterprise[s]?$/, '')                 // Enterprise
    .replace(/[,]? [Bb]an[ck]share[s]?$/, '')               // Bancshares
    .replace(/[,]? [Bb]ancorp[s]?$/, '')                    // Bancorp
    .replace(/[,]? [Bb]ancorporation[s]?$/, '')             // Bancorporation
    .replace(/[,]? [Ss]tock[s]?$/, '')                      // Stock
    .replace(/[,]? [Pp]ublic$/, '')                         // Public
    .replace(/[,&]? [Pp]artners$/, '')                      // Partners
    .replace(/ \([^)]*\)/g, '')                             // Anything inside parenthesis
    .replace(/[,]? P[.]?J[.]?S[.]?C[.]?$/, '')              // PJSC
    .replace(/[,]? K[.]?S[.]?C[.]?P[.]?$/, '')              // KSCP
    .replace(/[,]? S[. ]?A[. ]?B[. ]? de C[. ]?V[.]?$/, '') // SAB de CV
    .replace(/[,]? [Ss][.]?[Gg][.]?[Pp][.]?[Ss][.]?$/, '')  // SGPS
    .replace(/[,]? [Ss][.]?[Aa][.]?[Oo][.]?[Gg][.]?$/, '')  // SAOG
    .replace(/[,]? UCITS$/, '')                             // UCITS
    .replace(/[,]? REIT$/, '')                              // REIT
    .replace(/[,]? [Ii][Nn][Cc][.]?$/, '')                  // INC
    .replace(/[,]? [Pp][.]?[Ll][.]?[Cc][.]?$/, '')          // PLC
    .replace(/[,]? [Pp][.]?[Ss][.]?[Cc][.]?$/, '')          // PSC
    .replace(/[,]? [Ll][.]?[Tt][.]?[Dd][.]?$/, '')          // LTD
    .replace(/[,]? [Bb][.]?[Hh][.]?[Dd][.]?$/, '')          // BHD
    .replace(/[,]? [Ll][.]?[Ll][.]?[Cc][.]?$/, '')          // LLC
    .replace(/[,]? [Uu][.]?[Ss][.]?[Aa][.]?$/, '')          // USA
    .replace(/[,]? [Aa][./ ]?[Ss][./ ]?[Aa][.]?$/, '')      // ASA
    .replace(/[,]? [Ss][./ ]?[Aa][./ ]?[Aa][.]?$/, '')      // SAA
    .replace(/[,]? [Tt][./ ]?[Aa][./ ]?[Ss][.]?$/, '')      // TAS
    .replace(/[,]? [Tt][./ ]?[Bb][./ ]?[Kk][.]?$/, '')      // TBK
    .replace(/[,]? [Ee][.]?[Tt][.]?[Ff][.]?$/, '')          // ETF
    .replace(/[,]? [Jj][.]?[Ss][.]?[Cc][.]?$/, '')          // JSC
    .replace(/[,]? [Tt][.]?[Ee][.]?[Kk][.]?$/, '')          // JSC
    .replace(/[,]? [Uu][.]?[Dd][.]?[Ww][.]?$/, '')          // UDW
    .replace(/[,]? [Ii][.]?[Rr][.]?[Ll][.]?$/, '')          // IRL
    .replace(/[,]? [Ss][ ./][Pp][ ./][Aa][ ./]$/, '')       // S.P.A
    .replace(/[,]? SpA$/, '')                               // SpA
    .replace(/[,]? [Ll][.]?[Pp][.]?$/, '')                  // LP
    .replace(/[,]? [Ss][./ ]?[Aa][.]?$/, '')                // SA
    .replace(/[,]? [Hh][./ ]?[Ff][.]?$/, '')                // HF
    .replace(/[,]? [Ss][./ ]?[Ee][.]?$/, '')                // SE
    .replace(/[,]? [Bb][./ ]?[Mm][.]?$/, '')                // BM
    .replace(/[,]? [Aa][./ ]?[Ss][.]?$/, '')                // AS
    .replace(/[,]? [Aa][./ ]?[Bb][.]?$/, '')                // AB
    .replace(/[,]? [Pp][./ ]?[Tt][.]?$/, '')                // PT
    .replace(/[,]? [Aa][./ ]?[Dd][.]?$/, '')                // AD
    .replace(/[,]? [Aa][./ ]?[Gg][.]?$/, '')                // AG
    .replace(/[,]? [Nn][./ ]?[Vv][.]?$/, '')                // NV
    .replace(/[,]? [Bb][./ ]?[Vv][.]?$/, '')                // NV
    .replace(/[,]? [Nn][./ ]?[Ll][.]?$/, '')                // NL
    .replace(/[,]? [Gg][./ ]?[Pp][.]?$/, '')                // GP
    .replace(/[,]? [Cc][./ ]?[Aa][.]?$/, '')                // GP
    // .replace(/ of$/, '')                                    // Drop trailing of
    .replace(/ and$/, '')                                   // Drop trailing and
    .replace(/ the$/, '')                                   // Drop trailing the
    .replace(/[& ,.']*$/, '')                               // Drop any trailing &,.'
    .replace(/[,]/g, '')                                    // Drop commas
    .trim()
}
