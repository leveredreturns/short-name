/* global describe it */

/**
 * Dependencies
 */

const assert = require('assert')
const join = require('path').join
const shorten = require(join(__dirname, '..', 'lib', 'index.js'))

/**
 * Tests
 */

describe('shorten-name', () => {
  it('HP Inc. Common Stock -> HP', () => {
    assert.equal(shorten('HP Inc. Common Stock'), 'HP')
  })

  it('Twitter, Inc. Common Stock -> Twitter', () => {
    assert.equal(shorten('Twitter, Inc. Common Stock'), 'Twitter')
  })

  it('LinkedIn Corporation Class A Co -> LinkedIn', () => {
    assert.equal(shorten('LinkedIn Corporation Class A Co'), 'LinkedIn')
  })

  it('Gilead Sciences Inc. -> Gilead Sciences', () => {
    assert.equal(shorten('Gilead Sciences Inc.'), 'Gilead Sciences')
  })

  it('Ford Motor Company Common Stock -> Ford Motor', () => {
    assert.equal(shorten('Ford Motor Company Common Stock'), 'Ford Motor')
  })

  it('Cummins Inc. Common Stock -> Cummins', () => {
    assert.equal(shorten('Cummins Inc. Common Stock'), 'Cummins')
  })

  it('UnitedHealth Group Incorporated -> UnitedHealth', () => {
    assert.equal(shorten('UnitedHealth Group Incorporated'), 'UnitedHealth')
  })

  it('Citigroup, Inc. Common Stock -> Citigroup', () => {
    assert.equal(shorten('Citigroup, Inc. Common Stock'), 'Citigroup')
  })

  it('Yelp Inc. Class A Common Stock -> Yelp', () => {
    assert.equal(shorten('Yelp Inc. Class A Common Stock'), 'Yelp')
  })

  it('JP Morgan Chase & Co. Common St -> JP Morgan Chase', () => {
    assert.equal(shorten('JP Morgan Chase & Co. Common St'), 'JP Morgan Chase')
  })

  it('McCormick & Co. -> McCormick', () => {
    assert.equal(shorten('McCormick & Co.'), 'McCormick')
  })

  it('McCormick & Co -> McCormick', () => {
    assert.equal(shorten('McCormick & Co'), 'McCormick')
  })

  it('Sunoco Logistics Partners L.P. -> Sunoco Logistics', () => {
    assert.equal(shorten('Sunoco Logistics Partners L.P.'), 'Sunoco Logistics')
  })

  it('Sunoco Logistics Partners (publ) -> Sunoco Logistics', () => {
    assert.equal(shorten('Sunoco Logistics Partners (publ)'), 'Sunoco Logistics')
  })

  it('Bank of America Corporation -> Bank of America', () => {
    assert.equal(shorten('Bank of America Corporation'), 'Bank of America')
  })

  // it('Big Mess of Companies -> Big Mess', () => {
  //   assert.equal(shorten('Big Mess of Companies'), 'Big Mess')
  // })
})
